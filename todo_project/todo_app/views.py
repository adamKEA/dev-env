from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Todo
# Create your views here.


@login_required
def todos(request):
    """
    Rendering users todo on get and creates one on post
    """
    context = {
        'title': 'Todo List'
    }

    if request.method == "POST":
        todo = Todo(
            user=request.user,
            description=request.POST["description"]
        )
        todo.save()

    context['todos'] = Todo.objects.all().filter(user=request.user)
    return render(request, 'todo_app/todos.html', context)


@login_required
def delete_todo(request, todo_id):
    """
    Deletes a todo
    """
    Todo.objects.filter(id=todo_id).delete()
    return redirect('todo_app:todos')


@login_required
def update_todo(request, todo_id):
    """
    Updates a todo
    """
    todo = Todo.objects.get(id=todo_id)
    todo.completed = not todo.completed
    todo.save()
    return redirect('todo_app:todos')
