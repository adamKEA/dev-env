from django.apps import AppConfig


class TodoAppConfig(AppConfig):
    """
    Default setting
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'todo_app'
