from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from todo_app.models import Todo


def create_todo(user, description):
    """
    Used to create a todo to the temp database
    """
    Todo.objects.create(
        user=user,
        description=description
    )


def create_user(username, password):
    """
    Used to create a user to the temp database
    """
    User.objects.create_user(
        username=username,
        password=password
    )


class TestTodoViews(TestCase):
    """
    Test class for the views in todo_app
    """

    def setUp(self):
        """
        Setup setting up the data to be used in the other methods
        """
        create_user('mag', '123')
        create_user('adam', '123')

        user_one = User.objects.get(username='mag')
        user_two = User.objects.get(username='adam')

        create_todo(user_one, 'Get food')
        create_todo(user_one, 'Get something else')
        create_todo(user_two, 'Play football')

    def test_todo_list_view(self):
        """
        Testing responses when going to the main page with all users todos
        """
        # Unauthorized
        response = self.client.get(reverse("todo_app:todos"))
        self.assertEqual(response.status_code, 302)  # redirect

        # Authorized
        self.client.login(username='mag', password='123')
        response = self.client.get(reverse("todo_app:todos"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Get food')
        self.assertContains(response, 'Get something else')
        self.assertNotContains(response, 'Play football')

    def test_delete_todo(self):
        """
        Testing delete todos
        """
        self.client.login(username='adam', password='123')
        todo = Todo.objects.get(user=User.objects.get(username='adam'))

        response = self.client.get(
            reverse("todo_app:delete", kwargs={'todo_id': todo.id}))
        self.assertEqual(response.status_code, 302)  # redirect on delete

        response = self.client.get(reverse("todo_app:todos"))
        self.assertNotContains(response, 'Play football')
