from django.test import SimpleTestCase
from django.urls import reverse, resolve
from todo_app import views


class TestTodoUrls(SimpleTestCase):
    """
    Unit testing todo_app.urls
    """
    def test_todo_urls_todos(self):
        """
        Making sure that todo_app:todos points to the expected view function
        """
        url = reverse("todo_app:todos")
        self.assertEqual(resolve(url).func, views.todos)
