from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.contrib.auth.models import User
# Create your views here.


def login(request):
    """
    Handles login get and post requests
    """
    context = {
        'title': 'Login'
    }

    if request.method == "POST":
        user = authenticate(
            request,
            username=request.POST["username"],
            password=request.POST["password"]
        )

        if user:
            dj_login(request, user)
            return redirect("todo_app:todos")

    return render(request, 'accounts_app/login.html', context)


def sign_up(request):
    """
    Handles sign up get and post requests
    """
    context = {
        'title': 'Sign up'
    }

    if request.method == "POST":
        if not User.objects.filter(username=request.POST["username"]):
            User.objects.create_user(
                username=request.POST["username"],
                password=request.POST["password"],
            )
            return redirect("accounts_app:login")

    return render(request, 'accounts_app/sign-up.html', context)


def logout(request):
    """
    Handles logout get request
    """
    dj_logout(request)
    return redirect('accounts_app:login')
