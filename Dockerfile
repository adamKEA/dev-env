FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /usr/src/app
COPY requirements.txt .

RUN pip install -r requirements.txt

# CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]

COPY ./entrypoint.sh /

ENTRYPOINT [ "sh", "/entrypoint.sh" ]
